package net.iescierva.dam17.pythonsenseplus;

public class recyclerSensores {


        private String sensor;
        private int imagenId;

        public recyclerSensores(String sensor, int imagenId) {
            this.sensor = sensor;
            this.imagenId = imagenId;
        }

        public String getSensor() {
            return sensor;
        }

        public void setSensor(String sensor) {
            this.sensor = sensor;
        }

        public int getImagenId() {
            return imagenId;
        }

        public void setImagenId(int imagenId) {
            this.imagenId = imagenId;
        }
    }
