package net.iescierva.dam17.pythonsenseplus;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static androidx.navigation.Navigation.findNavController;

public class menuAparatos extends Fragment {

    public menuAparatos() {
        // Required empty public constructor
    }

    RequestQueue mQueue = null;
    ArrayList<String>listaNumAparatos = new ArrayList<>();
    ListView listaAparatos;
    JSONArray jsonArray = new JSONArray();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View vista = inflater.inflate(R.layout.fragment_menu_aparatos, container, false);

        listaAparatos = (ListView) vista.findViewById(R.id.idLista);

        listaAparatos.invalidateViews();

        jsonParse();

        return vista;
    }



    public void jsonParse(){

        mQueue = Volley.newRequestQueue(getContext());
        String url ="http://10.0.2.2:8000/pycom_rest/devices_names/";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    jsonArray = response.getJSONArray("data");
                    //arrayDatos.add(jsonArray.toString());
                    Log.d("json array",jsonArray.toString());

                    for(int i = 0; i < jsonArray.length(); i++){

                        String nombre = null;
                        try {

                            nombre = jsonArray.getString(i);
                            listaNumAparatos.add(nombre);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                    final ArrayAdapter adaptador = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, listaNumAparatos);

                    listaAparatos.setAdapter(adaptador);

                    listaAparatos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            try {

                                String nombre = jsonArray.getString(position);

                                Toast.makeText(parent.getContext(), nombre, Toast.LENGTH_SHORT).show();

                                adaptador.clear();

                                Bundle bundle = new Bundle();
                                bundle.putString("device", nombre);
                                findNavController(view).navigate(R.id.idMenuSensores, bundle);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });

        mQueue.add(request);



    }

}
