package net.iescierva.dam17.pythonsenseplus;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class Sensores extends Fragment {

    Bundle recibir = new Bundle();
    String sensor;
    String device;
    String value;
    JSONArray jsonArray = new JSONArray();
    RequestQueue mQueue = null;
    TextView tvValue;

    public Sensores() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_sensores, container, false);

        tvValue = (TextView) vista.findViewById(R.id.idValue);

        recibir = getArguments();
        sensor = recibir.getString("sensor");
        device = recibir.getString("device");

        Valores();

        return vista;
    }

    private void Valores() {


        mQueue = Volley.newRequestQueue(getContext());
        String url = "http://10.0.2.2:8000/pycom_rest/" + device + "/" + sensor + "/";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    jsonArray = response.getJSONArray("data");

                    JSONObject jsonObject = (JSONObject) jsonArray.get(0);

                    value = jsonObject.getString("Value");

                    if (sensor.equals("Temperature") | sensor.equals("Temperatura")){

                        tvValue.setText(value + "º");

                    }
                    else if (sensor.equals("Battery") | sensor.equals("Bateria")){

                        tvValue.setText(value + "%");

                    }
                    else {

                        tvValue.setText(value);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });

        mQueue.add(request);

    }
}