package net.iescierva.dam17.pythonsenseplus;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static androidx.navigation.Navigation.findNavController;

/**
 * The configuration screen for the {@link NewAppWidget NewAppWidget} AppWidget.
 */
public class NewAppWidgetConfigureActivity extends Activity {

    boolean existe = false;
    String aparato = "";
    String sensor = "";

    RequestQueue mQueue = null;
    ArrayList<String> listaNumAparatos = new ArrayList<>();
    ListView listaAparatos;
    ArrayAdapter adaptador;

    JSONArray jsonArray = new JSONArray();

    ArrayList<String> Provisional = new ArrayList<>();
    ArrayList<String> Sensores = new ArrayList<>();
    JSONObject jsonObject;

    private static final String PREFS_NAME = "net.iescierva.dam17.pythonsenseplus.NewAppWidget";
    private static final String PREF_PREFIX_KEY = "appwidget_";
    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    /*

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            final Context context = NewAppWidgetConfigureActivity.this;

            // When the button is clicked, store the string locally
            String widgetText = sensor;
            saveTitlePref(context, mAppWidgetId, widgetText);

            // It is the responsibility of the configuration activity to update the app widget
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            NewAppWidget.updateAppWidget(context, appWidgetManager, mAppWidgetId);

            // Make sure we pass back the original appWidgetId
            Intent resultValue = new Intent();
            resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
            setResult(RESULT_OK, resultValue);
            finish();
        }
    };

    */

    public NewAppWidgetConfigureActivity() {
        super();
    }

    // Write the prefix to the SharedPreferences object for this widget
    static void saveTitlePref(Context context, int appWidgetId, String text) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.putString(PREF_PREFIX_KEY + appWidgetId, text);
        prefs.apply();
    }

    // Read the prefix from the SharedPreferences object for this widget.
    // If there is no preference saved, get the default from a resource


    static String loadTitlePref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        String titleValue = prefs.getString(PREF_PREFIX_KEY + appWidgetId, null);

        if (titleValue != null) {
            return titleValue;
        } else {
            return context.getString(R.string.appwidget_text);
        }

    }


    static void deleteTitlePref(Context context, int appWidgetId) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.remove(PREF_PREFIX_KEY + appWidgetId);
        prefs.apply();
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if the user presses the back button.

        listaAparatos = (ListView) findViewById(R.id.idLista);

        setResult(RESULT_CANCELED);

        setContentView(R.layout.new_app_widget_configure);

        //findViewById(R.id.add_button).setOnClickListener(mOnClickListener);

        listaAparatos = findViewById(R.id.idList);

        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
            return;
        }

        //mAppWidgetText.setText(loadTitlePref(NewAppWidgetConfigureActivity.this, mAppWidgetId));

        jsonParse();

    }


    public void jsonParse(){

        mQueue = Volley.newRequestQueue(getApplicationContext());
        String url ="http://10.0.2.2:8000/pycom_rest/devices_names/";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    jsonArray = response.getJSONArray("data");

                    for(int i = 0; i < jsonArray.length(); i++){

                        String nombre = null;
                        try {

                            nombre = jsonArray.getString(i);
                            listaNumAparatos.add(nombre);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                    adaptador = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, listaNumAparatos);

                    listaAparatos.setAdapter(adaptador);

                    listaAparatos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            try {

                                aparato = jsonArray.getString(position);

                                adaptador.clear();

                                Sensores(aparato);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });

        mQueue.add(request);

    }


    private void Sensores(final String aparato) {

        mQueue = Volley.newRequestQueue(getApplicationContext());
        String url ="http://10.0.2.2:8000/pycom_rest/" + aparato + "/sensor_list/";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    jsonArray = response.getJSONArray("data");

                    for(int i = 0; i < jsonArray.length(); i++){

                        String nombre = null;
                        try {

                            jsonObject = (JSONObject) jsonArray.get(i);

                            nombre = jsonObject.getString("Sensor");

                            Provisional.add(nombre);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    for (int i = 0; i < Provisional.size(); i++){

                        for (int j = 0; j < Sensores.size(); j++){

                            if (Provisional.get(i).equals(Sensores.get(j))){

                                existe = true;
                                break;

                            }

                        }

                        if (existe == false){

                            Sensores.add(Provisional.get(i));

                        }

                        existe = false;
                    }

                    adaptador = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, Sensores);

                    listaAparatos.setAdapter(adaptador);

                    listaAparatos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            try {

                                sensor = Sensores.get(position);

                                adaptador.clear();


                                //Probar bucle infinito aqui----


                                Valores(aparato, sensor);


                                //--------------------------



                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });

        mQueue.add(request);

    }



    public void Valores(final String aparato, final String sensor) {


        mQueue = (RequestQueue) Volley.newRequestQueue(this);
        String url = "http://10.0.2.2:8000/pycom_rest/" + aparato + "/" + sensor + "/";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(JSONObject response) {

                try {

                    jsonArray = response.getJSONArray("data");

                    JSONObject jsonObject = (JSONObject) jsonArray.get(0);

                    String value = jsonObject.getString("Value");

                    String texto = "Valor del sensor " + sensor + " del dispositivo " + aparato + ": " + value;

                    Log.e(texto, "TEXTO");

                    final Context context = NewAppWidgetConfigureActivity.this;

                    // When the button is clicked, store the string locally

                    saveTitlePref(context, mAppWidgetId, texto);

                    // It is the responsibility of the configuration activity to update the app widget
                    AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                    NewAppWidget.updateAppWidget(context, appWidgetManager, mAppWidgetId);

                    // Make sure we pass back the original appWidgetId
                    Intent resultValue = new Intent();
                    resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
                    setResult(RESULT_OK, resultValue);

                    finish();




                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });

        mQueue.add(request);


    }


}