package net.iescierva.dam17.pythonsenseplus;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static androidx.navigation.Navigation.findNavController;

public class menu extends Fragment {

    boolean existe = false;
    String device;

    ArrayList<String> Provisional = new ArrayList<>();
    ArrayList<String> Sensores = new ArrayList<>();
    JSONArray jsonArray = new JSONArray();
    JSONObject jsonObject;

    RecyclerView recyclerSensores;
    sensoresAdapter adapter;

    TextView tvDevice;
    RequestQueue mQueue = null;
    Bundle recibir = new Bundle();


    ArrayList<recyclerSensores> listaSensores;

    public menu() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View vista = inflater.inflate(R.layout.fragment_menu, container, false);

        tvDevice = (TextView) vista.findViewById(R.id.tvDevide);
        recyclerSensores = (RecyclerView) vista.findViewById(R.id.recyclerId);
        recyclerSensores.setLayoutManager(new LinearLayoutManager(getContext()));

        listaSensores = new ArrayList<>();

        recibir = getArguments();
        device = recibir.getString("device");
        tvDevice.setText(device);

        Sensores();

        Sensores.clear();

        return vista;

    }

    private void Sensores() {

        mQueue = Volley.newRequestQueue(getContext());
        String url ="http://10.0.2.2:8000/pycom_rest/" + device + "/sensor_list/";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    jsonArray = response.getJSONArray("data");

                    for(int i = 0; i < jsonArray.length(); i++){

                        String nombre = null;
                        try {

                            jsonObject = (JSONObject) jsonArray.get(i);

                            nombre = jsonObject.getString("Sensor");

                            Provisional.add(nombre);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    for (int i = 0; i < Provisional.size(); i++){

                        for (int j = 0; j < Sensores.size(); j++){

                            if (Provisional.get(i).equals(Sensores.get(j))){

                                existe = true;
                                break;

                            }

                        }

                        if (existe == false){

                            Sensores.add(Provisional.get(i));

                        }

                        existe = false;
                    }

                    //for para recorrer array Sensores y elegir imagen que meter

                    for (int i = 0; i < Sensores.size(); i++){

                        Log.e(Sensores.get(i), "SENSORES");

                        if (Sensores.get(i).equals("Temperature") | Sensores.get(i).equals("Temperatura")) {

                            listaSensores.add(new recyclerSensores(Sensores.get(i), R.mipmap.imgtemperatura));

                        }

                    }

                    adapter = new sensoresAdapter(listaSensores);

                    adapter.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String sensor = listaSensores.get(recyclerSensores.getChildAdapterPosition(v)).getSensor();

                            Log.e(sensor, "SSENSORaPASAR");

                            Bundle bundle = new Bundle();
                            bundle.putString("sensor", sensor);
                            bundle.putString("device", device);

                            findNavController(v).navigate(R.id.idSensores, bundle);

                        }
                    });

                    recyclerSensores.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });

        mQueue.add(request);

    }

}