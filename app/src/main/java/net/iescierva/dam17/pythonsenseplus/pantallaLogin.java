package net.iescierva.dam17.pythonsenseplus;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static androidx.navigation.Navigation.findNavController;


/**
 * A simple {@link Fragment} subclass.
 */
public class pantallaLogin extends Fragment {

    private Button boton1;
    private Button boton2;
    public TextView txtName;
    public TextView txtPassword;


    public pantallaLogin() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View vista = inflater.inflate(R.layout.fragment_pantalla_login, container, false);

        boton1 = vista.findViewById(R.id.idMenu);
        boton2 = vista.findViewById(R.id.idRegister);
        txtName=(TextView)vista.findViewById(R.id.name);
        txtPassword=(TextView)vista.findViewById(R.id.password);

        boton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean existe = false;
                String nombre = txtName.getText().toString();
                String contraseña = txtPassword.getText().toString();

                try {
                    ArrayList arrayNombres = MainActivity.getInstance().SharedPreferencesGetNombre();
                    ArrayList arrayContraseñas = MainActivity.getInstance().SharedPreferencesGetContraseña();


                    if (arrayNombres != null & arrayContraseñas != null) {

                        for (int i = 0; i < arrayNombres.size(); i++) {

                            if (arrayNombres.get(i).equals(nombre)) {

                                if (arrayContraseñas.get(i).equals(contraseña)) {

                                    existe = true;

                                }
                            }
                        }

                    }

                }catch ( Exception e){}

              if(existe == true) {
                  findNavController(v).navigate(R.id.idMenuAparatos);
              }
              else{

                  Toast.makeText(getContext(), "Usuario o contraseña incorrecto", Toast.LENGTH_SHORT).show();
                  txtPassword.setText("");

              }
            }
        });

        boton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                findNavController(v).navigate(R.id.idRegister);
            }
        });

        return vista;
    }

}
