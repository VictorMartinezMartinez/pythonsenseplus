package net.iescierva.dam17.pythonsenseplus;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static androidx.navigation.Navigation.findNavController;


/**
 * A simple {@link Fragment} subclass.
 */
public class register extends Fragment {

    public TextView txtName;
    public TextView txtPassword;
    public TextView txtVerPassword;

    public register() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View vista = inflater.inflate(R.layout.fragment_register, container, false);

        txtName=(TextView)vista.findViewById(R.id.regName);
        txtPassword=(TextView)vista.findViewById(R.id.regPasword);
        txtVerPassword=(TextView)vista.findViewById(R.id.verPassword);

        Button boton1 = vista.findViewById(R.id.btnRegistering);


        boton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = txtName.getText().toString();
                String password = txtPassword.getText().toString();
                String verPassword = txtVerPassword.getText().toString();

                if(name.equals("") || password.equals("") || verPassword.equals("")) {

                    txtPassword.setText("");
                    txtVerPassword.setText("");

                    Toast.makeText(getContext(), "Usuario o Contraseña no legibles", Toast.LENGTH_SHORT).show();



                }

                else if(!name.equals("") & !password.equals("") & !verPassword.equals("") & password.equals(verPassword) & password.length() < 6){

                    txtPassword.setText("");
                    txtVerPassword.setText("");

                    Toast.makeText(getContext(), "La contraseña debe tener mas de 6 caracteres", Toast.LENGTH_SHORT).show();

                }

                else if(!name.equals("") & !password.equals("") & !verPassword.equals("") & !password.equals(verPassword)){

                    txtPassword.setText("");
                    txtVerPassword.setText("");

                    Toast.makeText(getContext(), "La contraseña no coincide", Toast.LENGTH_SHORT).show();

                }

                else {

                   boolean existe = MainActivity.getInstance().SharedPreferencesSet(name, password);

                    if (existe == false) {

                        findNavController(v).navigate(R.id.idLogin);

                    }

                }
            }
        });

        return vista;

    }

}
