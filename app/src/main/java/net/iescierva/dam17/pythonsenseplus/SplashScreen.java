package net.iescierva.dam17.pythonsenseplus;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class SplashScreen extends Activity {

    VideoView videoView;
    MediaPlayer sonido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        videoView = (VideoView) findViewById(R.id.videoView);

        sonido = MediaPlayer.create(SplashScreen.this,R.raw.sonidointro);
        sonido.start();

        String ruta = "android.resource://" + getPackageName() + "/" + R.raw.videoapp;
        videoView.setVideoURI(Uri.parse(ruta));
        videoView.start();

        //Uri uri = Uri.parse("");
        //videoView.setMediaController(new MediaController(this));
        //videoView.setVideoURI(uri);
        //videoView.requestFocus();
        //videoView.start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(SplashScreen.this, MainActivity.class);

                startActivity(intent);
                finish();
                sonido.pause();

            }
        }, 6000);

    }
}
