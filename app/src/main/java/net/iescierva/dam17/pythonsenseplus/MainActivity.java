package net.iescierva.dam17.pythonsenseplus;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    private static MainActivity myself;

    public MainActivity() {
        myself = this;
    }

    public static MainActivity getInstance() {
        return myself;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public boolean SharedPreferencesSet(String nombre, String contraseña){

        Boolean existe = false;

        SharedPreferences sharpref = getSharedPreferences("dtoLogin", this.MODE_PRIVATE);

        String nombres = sharpref.getString("nombres", "");
        String[] nombresSplit = nombres.split(",");

        for (int i = 0; i < nombresSplit.length; i++){

            String comNom = nombresSplit[i];

            if(comNom.equals(nombre)) {

                existe = true;
                break;
            }
        }

        if (existe == false) {

            String contraseñas = sharpref.getString("contraseñas", "");

            nombres = nombres + nombre + ",";
            contraseñas = contraseñas + contraseña + ",";

            SharedPreferences.Editor editor = sharpref.edit();
            editor.putString("nombres", nombres);
            editor.putString("contraseñas", contraseñas);
            editor.commit();

            Toast.makeText(this, "Usuario creado con exito", Toast.LENGTH_SHORT).show();



        }
        else{

            Toast.makeText(this, "El nombre de usuario ya existe", Toast.LENGTH_SHORT).show();


        }

        return existe;

    }

    public ArrayList<String> SharedPreferencesGetNombre(){

        ArrayList<String> arrayNombres = new ArrayList<>();

        SharedPreferences sharpref = getSharedPreferences("dtoLogin", this.MODE_PRIVATE);

        String nombres = sharpref.getString("nombres", "");
        String[] nombresSplit = nombres.split(",");

        for (int i = 0; i<nombresSplit.length; i++){

            String addNombre = nombresSplit[i];

            arrayNombres.add(addNombre);

        }

        return arrayNombres;

    }

    public ArrayList<String> SharedPreferencesGetContraseña(){

        ArrayList<String> arrayContraseñas = new ArrayList<>();

        SharedPreferences sharpref = getSharedPreferences("dtoLogin", this.MODE_PRIVATE);

        String contraseñas = sharpref.getString("contraseñas", "");
        String[] contraseñasSplit = contraseñas.split(",");

        for (int i = 0; i<contraseñasSplit.length; i++){

            String addContraseña = contraseñasSplit[i];

            arrayContraseñas.add(addContraseña);

        }

        return arrayContraseñas;

    }


}
